Categories:Games
License:Apache-2.0
Web Site:
Source Code:
Issue Tracker:

Auto Name:Solitaire
Summary:Solitaire collection
Description:
Solitaire Collection of Klondike (Regular solitaire), Spider Solitaire, and
Freecell using the touchscreen interface. Features include multi-level undo,
animated card movement, and statistic/score tracking.
.

Build:1.12.2,450
    commit=30
    prebuild=rm build.xml
    target=android-8

Auto Update Mode:None
Update Check Mode:Static
Current Version:1.12.2
Current Version Code:450

No Source Since:1.12.3
